package com.alexkamenev.fragments.packages;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alexkamenev.fragments.R;

/**
 * Created by hug3m on 14.09.2016.
 */
public class FragmentOne extends Fragment implements View.OnClickListener {
    Button delete;
    Button dontpress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);

        delete = (Button) view.findViewById(R.id.ButtonFirstFragment2);
        dontpress = (Button) view.findViewById(R.id.ButtonFirstFragment1);
        delete.setOnClickListener(this);
        dontpress.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment frag1 = getFragmentManager().findFragmentById(R.id.frame2);
        TextView myTextView = (TextView)getActivity().findViewById(R.id.tv_status);
        switch (v.getId()) {


            case R.id.ButtonFirstFragment1:
                v.setVisibility(View.GONE);
                myTextView.setText("Ой! Кнопка была удалена.");

                break;
            case R.id.ButtonFirstFragment2:
                transaction.remove(frag1);
                myTextView.setText("Вы удалили другой фрагмент");

                break;


        }
        transaction.commit();
    }
}
