package com.alexkamenev.fragments.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.alexkamenev.fragments.R;
import com.alexkamenev.fragments.packages.FragmentOne;
import com.alexkamenev.fragments.packages.FragmentTwo;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FragmentOne m_FirstFragment = null;
    private FragmentTwo m_SecondFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        View btnAddFirstFragment = findViewById(R.id.AddFirstFragment);
        btnAddFirstFragment.setOnClickListener(this);
        View btnAddSecondFragment = findViewById(R.id.AddSecondFragment);
        btnAddSecondFragment.setOnClickListener(this);
        View btnAddTwo = findViewById(R.id.AddTwoFragments);
        btnAddTwo.setOnClickListener(this);

        m_FirstFragment = new FragmentOne();
        m_SecondFragment = new FragmentTwo();

    }



    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        TextView myTextView = (TextView) findViewById(R.id.tv_status);
        switch (view.getId()) {

            case R.id.AddFirstFragment:
                transaction.replace(R.id.frame1, m_FirstFragment);

                myTextView.setText("Вы добавили первый фрагмент");

                break;
            case R.id.AddSecondFragment:
                transaction.replace(R.id.frame2, m_SecondFragment);

                myTextView.setText("Вы добавили второй фрагмент");

                break;
            case R.id.AddTwoFragments:
                transaction.replace(R.id.frame1, m_FirstFragment);
                transaction.replace(R.id.frame2, m_SecondFragment);
                myTextView.setText("Вы добавили оба фрагмента");

                break;

        }
        transaction.commit();

    }
}