package com.alexkamenev.mycash.tools_and_constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {


    private static final String KEY_BOOLEAN_IS_FIRST_START = "KEY_BOOLEAN_IS_FIRST_START";
    private static final String KEY_BOOLEAN_PASSWORD_CHECK = "KEY_BOOLEAN_PASSWORD_CHECK";

    private SharedPreferences m_SharedPreferences = null;

    // getDefaultSharedPreferences returns SharedPreferences objects and let's us use methods


    public AppSettings(Context context) {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    //First start of application
    public boolean getIsFirstStart() {
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_FIRST_START, true);
    }

    public void setIsFirstStart(boolean bIsFirstStart) {
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_FIRST_START, bIsFirstStart);
        editor.commit();
    }

    public boolean passwordCheck()
    {
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_PASSWORD_CHECK, false);
    }
}