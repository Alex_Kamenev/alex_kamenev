package com.alexkamenev.mycash.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by hug3m on 16.01.2017.
 */

public class DBWrapper {
    private DBHelper m_DbHelper = null;
    private String m_strTableName = "";

    public DBWrapper(Context context, String strTableName){
        m_DbHelper = new DBHelper(context);
        m_strTableName = strTableName;
    }


    protected SQLiteDatabase getReadable(){
        return m_DbHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritable(){
        return m_DbHelper.getWritableDatabase();
    }

    protected String getTableName(){
        return m_strTableName;
    }
}
