package com.alexkamenev.mycash.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hug3m on 13.01.2017.
 */

public abstract class Note implements Serializable {
    private long timestamp;
    protected Wallet wallet;
    protected String purpose;
    protected double amount;
    public Note(Wallet wallet,String purpose,double amount){
        this.wallet = wallet;
        this.purpose = purpose;
        this.amount = amount;
        this.timestamp = System.currentTimeMillis();
    }


    public double getAmount() {
        return amount;
    }

    public String getPurpose() {
        return purpose;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getFormattedTime(){
        return new SimpleDateFormat("dd.MM.yyyy").format(new Date(timestamp));
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public abstract int getColor();
    public abstract String getType();
    public abstract String getDesName();
    public abstract void onDelete();
    public abstract int getImage();
}
