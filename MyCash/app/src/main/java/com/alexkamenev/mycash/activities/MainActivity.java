package com.alexkamenev.mycash.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.alexkamenev.mycash.R;
import com.alexkamenev.mycash.fragments.AboutUsFragment;
import com.alexkamenev.mycash.fragments.HistoryFragment;
import com.alexkamenev.mycash.fragments.WalletFragment;
import com.alexkamenev.mycash.model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity{

    private static final String TAG = "my_tag";
    boolean doubleBackToExitPressedOnce = false;

    boolean isPause;
    private Intent intent;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(getClass().getName(), "onCreate: ");
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        User.getInstance().loadFile(this);
        intent = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle(getResources().getString(R.string.app_name));

        setupToolbar();

//        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
//                this,  mDrawerLayout, mToolbar,
//                R.string.navigation_drawer_open, R.string.navigation_drawer_close
//        );
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        mDrawerToggle.syncState();
//
        setupViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new WalletFragment(), getResources().getString(R.string.Wallet));
        adapter.addFragment(new HistoryFragment(), getResources().getString(R.string.History));
        adapter.addFragment(new AboutUsFragment(), getResources().getString(R.string.About));
        viewPager.setAdapter(adapter);
    }

    private void setupToolbar(){
        getDelegate().getSupportActionBar().setDisplayShowTitleEnabled(true); //optional
    }
//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        User.getInstance().saveFile(this);
//        int id = item.getItemId();
//        if (id == R.id.nav_wallet) {
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//           // transaction.replace(R.id.place_fragement,walletFragment);
//            transaction.commit();
//        }
//        else if (id == R.id.nav_settings) {
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//          //  transaction.replace(R.id.place_fragement,settingFragment);
//            transaction.commit();
//        } else if (id == R.id.nav_about_us) {
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//           // transaction.replace(R.id.place_fragement, aboutusFragment);
//            transaction.commit();
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(getClass().getName(), "onBackPressed: ");
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Для выхода нажмите кнопку "+"Назад"+" еще раз", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(getClass().getName(), "onPause: ");
        User.getInstance().saveFile(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getClass().getName(), "onResume: ");

    }

    @Override
    protected void onStop() {
        super.onStop();
        User.getInstance().saveFile(this);
        isPause=true;
        Log.d(getClass().getName(), "onStop: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(getClass().getName(), "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(getClass().getName(), "onDestroy: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(getClass().getName(), "onStart: ");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateUi() {
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(intent);
        }


}