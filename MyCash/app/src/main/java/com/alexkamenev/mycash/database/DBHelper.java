package com.alexkamenev.mycash.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hug3m on 16.01.2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    /**
     * Конструктор класса. Для открытия БД используется Context.
     * @param context
     */
    public DBHelper(Context context) {
        /*В качестве параметров в конструктор родителя дополнительно передаем
        * имя базы данных (их может быть не одна) и ее (базы данных) версию.*/
        super(context, "db_example", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.TABLE_INCOME + " ("
                + DBConstants.TABLE_INCOME_FIELD_MONEY_EARNED + " TEXT NOT NULL); ");

        db.execSQL("CREATE TABLE " + DBConstants.TABLE_SPENDINGS + " ("
                + DBConstants.TABLE_SPENDINGS_FIELD_ID + " INTEGER PRIMARY KEY NOT NULL, "
                + DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT + " TEXT NOT NULL, "
                + DBConstants.TABLE_SPENDINGS_FIELD_COMMENT + " TEXT NOT NULL, "
                + DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE + " TEXT NOT NULL); ");


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}