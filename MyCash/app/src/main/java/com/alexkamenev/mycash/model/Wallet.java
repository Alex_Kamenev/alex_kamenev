package com.alexkamenev.mycash.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Wallet implements Serializable {
    private String name;
    private double maxBalance;
    private double balance;
    private List<Note> notes;
    private Calendar createDate;
    public Wallet(String name,double maxBalance){
        this.name = name;
        this.maxBalance = maxBalance;
        this.balance = maxBalance;
        notes = new ArrayList<Note>();
        createDate = Calendar.getInstance();
        createDate.set(Calendar.HOUR_OF_DAY,0);
        createDate.set(Calendar.MINUTE,0);
        createDate.set(Calendar.SECOND,0);
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getMoney(String purpose, double amount){
        balance+=amount;
        if (balance>maxBalance) maxBalance=balance;
        notes.add(new GetMoneyNote(this,purpose,amount));
    }

    public  boolean payProducts(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new ProductsNote(this,purpose,amount));
        return true;
    }

    public  boolean payEntertainment(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new EntertainmentNote(this,purpose,amount));
        return true;
    }

    public  boolean payTransport(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new TransportNote(this,purpose,amount));
        return true;
    }

    public  boolean payCafe(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new CafeNote(this,purpose,amount));
        return true;
    }

    public  boolean payBadHabits(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new BadHabitsNote(this,purpose,amount));
        return true;
    }

    public  boolean payClothes(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new ClothesNote(this,purpose,amount));
        return true;
    }

    public  boolean payHealth(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new HealthNote(this,purpose,amount));
        return true;
    }

    public  boolean payLink(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new LinkNote(this,purpose,amount));
        return true;
    }

    public  boolean payOther(String purpose, double amount){
        if (balance-amount<0) return false;
        balance-=amount;
        notes.add(new OtherNote(this,purpose,amount));
        return true;
    }


    @Override
    public String toString() {
        return name;
    }

    public void setBalance(double balance) {
        if (balance>maxBalance) maxBalance = balance;
        this.balance = balance;
    }

    void removeNote(Note note){
        notes.remove(note);
    }

    public List<Note> getNotes() {
        return notes;
    }
}
