package com.alexkamenev.mycash.model;

import com.alexkamenev.mycash.R;

/**
 * Created by hug3m on 13.01.2017.
 */

public class PayMoneyNote extends Note{
    public PayMoneyNote(Wallet wallet, String purpose, double amount) {
        super(wallet, purpose, amount);
    }

    @Override
    public int getColor() {
        return android.R.color.holo_red_light;
    }

    @Override
    public String getType() {
        return "Расходы";
    }

    @Override
    public String getDesName() {
        return null;
    }

    @Override
    public void onDelete() {
        wallet.setBalance(wallet.getBalance()+amount);
        wallet.removeNote(this);
    }

    @Override
    public int getImage() {
        return R.drawable.wallet;
    }
}
