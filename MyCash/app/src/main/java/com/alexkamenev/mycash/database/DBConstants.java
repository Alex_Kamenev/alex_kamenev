package com.alexkamenev.mycash.database;

/**
 * Created by hug3m on 16.01.2017.
 */

public class DBConstants {




    public static final String TABLE_INCOME = "Income";
    public static final String TABLE_INCOME_FIELD_MONEY_EARNED = "_earned";

    public static final String TABLE_SPENDINGS = "Spendings";
    public static final String TABLE_SPENDINGS_FIELD_MONEY_SPENT = "_spendings";
    public static final String TABLE_SPENDINGS_FIELD_COMMENT = "_comment";
    public static final String TABLE_SPENDINGS_FIELD_PURPOSE = "_purpose";
    public static final String TABLE_SPENDINGS_FIELD_ID = "_id";


}