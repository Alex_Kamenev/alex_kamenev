package com.alexkamenev.mycash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexkamenev.mycash.R;
import com.alexkamenev.mycash.model.Note;

import java.util.List;

public class NoteListViewAdapter extends ArrayAdapter<Note> {
    public NoteListViewAdapter(Context context, int resource, List<Note> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.note_cell, null);
        }
        Note note = getItem(position);

        view.setBackgroundResource(note.getColor());
        ImageView category = (ImageView) view.findViewById(R.id.imgCategory);
        category.setImageResource(note.getImage());
        TextView typeText = (TextView) view.findViewById(R.id.note_type_text);
        typeText.setText(note.getType());
        TextView purposeText = (TextView) view.findViewById(R.id.note_purpose_text);
        purposeText.setText(note.getPurpose());

        TextView amountText = (TextView) view.findViewById(R.id.note_amount_text);
        amountText.setText(String.format("%.2f",note.getAmount()));

        if(note.getType().equals("Доходы")){
            amountText.setTextColor(amountText.getResources().getColor(R.color.colorPrimaryDark));
        }else {
            amountText.setTextColor(amountText.getResources().getColor(R.color.colorAccent));
        }

        TextView dateText = (TextView) view.findViewById(R.id.note_date_text);
        dateText.setText(note.getFormattedTime());
        return view;
    }

    @Override
    public Note getItem(int position) {
        return super.getItem(getCount()-position-1);
    }
}