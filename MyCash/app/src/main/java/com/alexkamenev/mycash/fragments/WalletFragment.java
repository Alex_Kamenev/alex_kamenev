package com.alexkamenev.mycash.fragments;

/**
 * Created by hug3m on 13.01.2017.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alexkamenev.mycash.R;
import com.alexkamenev.mycash.adapters.WalletRecyclerViewAdapter;
import com.alexkamenev.mycash.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletFragment extends Fragment {

    @BindView(R.id.wallet_list)
    RecyclerView walletRecyclerView;
    private WalletRecyclerViewAdapter walletAdapter;
    public WalletFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wallet, container, false);
        ButterKnife.bind(this,rootView);
        setUpRecyclerView();

//        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
//
//        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);

        return rootView;
    }

    private void setUpRecyclerView(){
        walletRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager  layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        walletRecyclerView.setLayoutManager(layoutManager);
        User u = User.getInstance();
        walletAdapter = new WalletRecyclerViewAdapter(getActivity(),u.getWallets());
        walletRecyclerView.setAdapter(walletAdapter);
    }


}
