package com.alexkamenev.mycash.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexkamenev.mycash.HelperListener;
import com.alexkamenev.mycash.R;
import com.alexkamenev.mycash.activities.LoginActivity;
import com.alexkamenev.mycash.model.User;
import com.alexkamenev.mycash.tools_and_constants.AppSettings;
import com.alexkamenev.mycash.views.PasscodeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hug3m on 15.01.2017.
 */

public class SettingsFragment extends Fragment  {

    @BindView(R.id.reset_button)
    Button resetButton;
    @BindView(R.id.edit_username_button) Button usernameButton;
    @BindView(R.id.edit_password_button) Button passwordButton;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.checkBox)
    CheckBox password;

    private String newpass;
    public SettingsFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  rootview  = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this,rootview);

        initButton();
        return rootview;
    }

    private void initButton(){
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputPassDialog(new HelperListener() {
                    @Override
                    public void onAction() {
                        showResetDialog();
                    }
                },false);
            }
        });
        usernameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputPassDialog(new HelperListener() {
                    @Override
                    public void onAction() {
                        showUsernameEditDialog();

                        User.getInstance().saveFile(getActivity());
                    }
                },false);
            }
        });
        passwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputPassDialog(new HelperListener() {
                    @Override
                    public void onAction() {
                        showInputPassDialog(new HelperListener() {
                            @Override
                            public void onAction() {
                                User.getInstance().setPasscode(newpass);
                                User.getInstance().saveFile(getActivity());
                            }
                        },true);
                    }
                },false);
            }
        });
        newpass = "";
        password.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(password.isChecked()){
                    boolean check;
                    check = true;
                    AppSettings appSettings = new AppSettings(getContext());
                    Preference preference = new Preference(getContext());
                    preference.isEnabled();


                    Intent intent = new Intent(getActivity(),LoginActivity.class);
                    intent.putExtra("check", check);
                }

            }
        });
    }

    private void showInputPassDialog(final HelperListener helper,boolean isCheck){
        DatePickerDialog.Builder builder = new DatePickerDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.passcode_dialog,null);
        final PasscodeView passcodeView = (PasscodeView) view.findViewById(R.id.passcode);
        passcodeView.setCheckMatch(isCheck);
        builder.setView(view);
        final Dialog dialog = builder.create();
        passcodeView.setHelperListener(new HelperListener() {
            @Override
            public void onAction() {
                if (passcodeView.isMatch()) {
                    newpass = passcodeView.getPasscode();
                    helper.onAction();
                    dialog.cancel();
                }
            }
        });
        dialog.show();
    }

    private void showResetDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Вы подтверждаете сброс?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        User.getInstance().reset();
                        User.getInstance().saveFile(getActivity());




                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }

    private void showUsernameEditDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.edit_wallet_dialog,null);
        TextView nameText = (TextView) view.findViewById(R.id.wallet_name_input);
        nameText.setHint("Введите новое имя");
        builder.setView(view)
                .setPositiveButton("Подтвердить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TextView nameText = (TextView) ((Dialog)dialog).findViewById(R.id.wallet_name_input);
                        if (nameText.getText().toString().equals("")){
                            showMessegeDialog("Введте имя!");
                            return;
                        }
                        User.getInstance().setUsername(nameText.getText().toString());
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }

    private void showMessegeDialog(String messege){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(messege)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }
}
