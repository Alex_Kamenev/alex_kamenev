package com.alexkamenev.mycash.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alexkamenev.mycash.R;
import com.alexkamenev.mycash.database.DBConstants;
import com.alexkamenev.mycash.database.DBHelper;
import com.alexkamenev.mycash.model.Note;
import com.alexkamenev.mycash.model.User;
import com.alexkamenev.mycash.model.Wallet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hug3m on 13.01.2017.
 */

public class WalletRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Wallet> wallets;
    private Context context;
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;
    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvBalance)
        TextView balance;
        @BindView(R.id.note_listview)
        ListView noteListView;
        @BindView(R.id.imgProducts)
        ImageView products;
        @BindView(R.id.imgEntertainment)
        ImageView entertainment;
        @BindView(R.id.imgTransport)
        ImageView transport;
        @BindView(R.id.imgCafe)
        ImageView cafe;
        @BindView(R.id.imgBadHabits)
        ImageView badhabits;
        @BindView(R.id.imgClothes)
        ImageView clothes;
        @BindView(R.id.imgHealth)
        ImageView health;
        @BindView(R.id.imgLink)
        ImageView link;
        @BindView(R.id.imgOther)
        ImageView other;
        @BindView(R.id.btnGetMoney)
        Button getMoney;

        public ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public WalletRecyclerViewAdapter(Context context,List<Wallet> wallets){
        this.context = context;
        this.wallets = wallets;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.wallet_layout, parent, false);
            return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemHolder = (ItemViewHolder) holder;
            final Wallet wallet = wallets.get(position);
            itemHolder.balance.setText(String.format("%.2f",wallet.getBalance()));


            final NoteListViewAdapter noteListViewAdapter = new NoteListViewAdapter(context,R.layout.note_cell,wallet.getNotes());
            itemHolder.noteListView.setAdapter(noteListViewAdapter);
            itemHolder.noteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Note note = noteListViewAdapter.getItem(position);
                    showNoteDetialDialog(context,note);
                }
            });

            itemHolder.products.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProductsDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.entertainment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEntertainmentDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.transport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTransportDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.cafe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCafeDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.badhabits.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showBadHabitsDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.clothes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showClothesDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.health.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHealthDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showShowLinkDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.other.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showOtherDialog(context,wallet,noteListViewAdapter);
                }
            });
            itemHolder.getMoney.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    showGetMoneyDialog(context,wallet,noteListViewAdapter);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return wallets.size();
    }

    private void showGetMoneyDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        final View getDialogView = inflater.inflate(R.layout.operation_dialog,null);

        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(getDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;
                        TextView amountGetText = (TextView) dialogBox.findViewById(R.id.operation_input);

                        String amountStr = amountGetText.getText().toString();

                        values.put(DBConstants.TABLE_INCOME_FIELD_MONEY_EARNED,amountStr);
                        db.insert(DBConstants.TABLE_INCOME,null,values);


                        dbHelper.close();
                        db.close();

                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.operation_purpose);
                        String purposeStr = purposeText.getText().toString();
                        if (purposeStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.enter_comment));
                            return;
                        }
                        wallet.getMoney(purposeStr,Double.parseDouble(amountStr));
                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        getDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

    }

    private void showOtherDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Other);
        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;
                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        TextView tvComment = (TextView) dialogBox.findViewById(R.id.tvComment);

                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT,amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }

                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Other);

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);

                        if(!wallet.payOther(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showProductsDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Products);
        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;
                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);



                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Products);




                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payProducts(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }


    private void showNoteDetialDialog(final Context context, final Note note){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.note_detial_dialog,null);
        LinearLayout head = (LinearLayout) view.findViewById(R.id.note_head);
        head.setBackgroundResource(note.getColor());
        TextView noteType = (TextView) view.findViewById(R.id.note_type_text);
        TextView noteDetailText = (TextView) view.findViewById(R.id.note_detail_text);
        noteType.setText(note.getType());
        String noteDetail = String.format(""+ String.valueOf(R.string.amount) +" : %.2f\n"+ String.valueOf(R.string.date) +
                " : %s\n" + String.valueOf(R.string.comment) + " : %s",note.getAmount(),note.getFormattedTime(),note.getPurpose());
        if (note.getDesName()!=null) noteDetail+="\nDestination "+note.getDesName();
        noteDetailText.setText(noteDetail);
        builder.setView(view)
                .setPositiveButton(R.string.Edit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;
                        showEditNoteDialog(dialogBox.getContext(),note);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        final Dialog dialog = builder.create();
        Button deleteNoteButton = (Button) view.findViewById(R.id.note_delete_button);
        deleteNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                note.onDelete();
                notifyChanged();
                dialog.cancel();
            }
        });
        dialog.show();
    }

        private void showEditNoteDialog(final Context context, final Note note){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.note_edit_dialog,null);
        EditText editPurposeText = (EditText) view.findViewById(R.id.edit_purpose_input);
        editPurposeText.setText(note.getPurpose());
        builder.setView(view)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;
                        EditText editPurposeText = (EditText) dialogBox.findViewById(R.id.edit_purpose_input);
                        String purposeStr = editPurposeText.getText().toString();
                        if (purposeStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.enter_comment));
                            return;
                        }
                        note.setPurpose(editPurposeText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }

    private void showMessegeDialog(Context context,String messege){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(messege)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }

    private void showEntertainmentDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Entertainment);
        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;

                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }

                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Entertainment);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payEntertainment(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showTransportDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Transport);

        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;



                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Transport);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payTransport(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showCafeDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Cafe);

        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;



                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Cafe);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payCafe(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showBadHabitsDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);

        spendings.setText(R.string.BadHabits);
        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;



                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.BadHabits);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payBadHabits(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showClothesDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        final View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Clothes);

        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;

                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Clothes);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payClothes(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showHealthDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Health);

        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;



                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);
                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Health);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if(!wallet.payHealth(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showShowLinkDialog(final Context context, final Wallet wallet, final NoteListViewAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View payDialogView = inflater.inflate(R.layout.spendings_dialog,null);
        TextView spendings = (TextView) payDialogView.findViewById(R.id.tvSpendings);
        spendings.setText(R.string.Link);

        final DBHelper dbHelper = new DBHelper(context);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final ContentValues values = new ContentValues();

        builder.setView(payDialogView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog dialogBox = (Dialog)dialog;
                        TextView amountPayText = (TextView) dialogBox.findViewById(R.id.spendings_input);



                        String amountStr = amountPayText.getText().toString();

                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_MONEY_SPENT, amountStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);


                        if (amountStr.equals("")) {
                            showMessegeDialog(context,String.valueOf(R.string.please_input_amount_of_money_in_your_wallet));
                            return;
                        }
                        TextView purposeText = (TextView) dialogBox.findViewById(R.id.spendings_purpose);
                        String purposeStr = purposeText.getText().toString();
                        String historyCat = context.getString(R.string.Link);


                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_PURPOSE, historyCat);
                        values.put(DBConstants.TABLE_SPENDINGS_FIELD_COMMENT, purposeStr);
                        db.insert(DBConstants.TABLE_SPENDINGS,null,values);



                        if(!wallet.payLink(purposeStr,Double.parseDouble(amountStr))){
                            showMessegeDialog(context,String.valueOf(R.string.not_enough_money));
                            return;
                        }

                        dbHelper.close();
                        db.close();

                        notifyChanged();
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
        payDialogView.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }


    private void notifyChanged(){
        notifyDataSetChanged();
        User.getInstance().saveFile(context);
    }

}