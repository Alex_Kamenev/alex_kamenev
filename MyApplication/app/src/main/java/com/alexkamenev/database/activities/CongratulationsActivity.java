package com.alexkamenev.database.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.alexkamenev.database.R;
import com.alexkamenev.database.adapters.RecordAdapter;
import com.alexkamenev.database.model.Record;
import com.alexkamenev.database.model.engine.RecordEngine;

import java.util.ArrayList;

public class CongratulationsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener
{
    private ListView m_ListView = null;
    private RecordAdapter m_adapter = null;
    private ArrayList<Record> m_arrData = new ArrayList<>();

    private String m_strSearchString = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_congratulations);

        EditText editText = (EditText) findViewById(R.id.SearchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSearchString = s.toString();
                updateList();
            }
        });

        m_ListView = (ListView) findViewById(R.id.listViewCongratulationsActivity);
        m_adapter = new RecordAdapter(m_arrData);
        m_ListView.setAdapter(m_adapter);
        m_ListView.setOnItemClickListener(this);
        View btnAdd = findViewById(R.id.addButtonCongratulationsActivity);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.removeAllButtonCongratulationsActivity);
        btnRemoveAll.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList() {


        RecordEngine engine = new RecordEngine(this);
        ArrayList<Record> arrData = engine.getAll();

        if (m_strSearchString.isEmpty()) {
            arrData = engine.getAll();
        } else {
            arrData = engine.getSearch(m_strSearchString);
        }

        m_arrData.clear();
        m_arrData.addAll(arrData);
        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addButtonCongratulationsActivity:
                Intent intent = new Intent(this, ComposeActivity.class);
                startActivity(intent);
                break;
            case R.id.removeAllButtonCongratulationsActivity:
                RecordEngine engine = new RecordEngine(this);
                engine.deleteAll();
                updateList();
                break;

        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentEdit = new Intent(this, ComposeActivity.class);
        intentEdit.putExtra(ComposeActivity.EXTRA_KEY_ID, id);
        startActivity(intentEdit);
    }
}
