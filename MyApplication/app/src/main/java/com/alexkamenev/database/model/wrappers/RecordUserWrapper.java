package com.alexkamenev.database.model.wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alexkamenev.database.model.User;
import com.alexkamenev.database.tools_and_constants.DBConstants;

import java.util.ArrayList;


public class RecordUserWrapper extends BaseDBWrapper {

    public RecordUserWrapper(Context context) {
        super(context, DBConstants.TABLE2_NAME);
    }

    public ArrayList<User> getAllUsers(){
        ArrayList<User> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        User userInfo = new User(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public User getItemById(long nId){
        User userInfo = null;
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_USERS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    userInfo = new User(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return userInfo;
    }

    public void insertUserItem(User item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        db.insert(getTableName(),null,values);
        db.close();
    }

    public void updateUserItem(User item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        String strSelection = DBConstants.TABLE_USERS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.update(getTableName(),values,strSelection, arrSelectionArgs);
        db.close();
    }

    public void deleteUserItem(User item){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_USERS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }

    public ArrayList<User> getSearch(String strSearch){
        String strLocalSearch = "%" + strSearch + "%";
        ArrayList<User> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        StringBuilder selectionBuilder = new StringBuilder();
        selectionBuilder.append(DBConstants.TABLE_USERS_FIELD_FIRSTNAME)
                .append(" LIKE ? OR ")
                .append(DBConstants.TABLE_USERS_FIELD_LASTNAME)
                .append(" LIKE ? OR ")
                .append(DBConstants.TABLE_USERS_FIELD_LASTNAME)
                .append(" LIKE ?");
        String strSelection = selectionBuilder.toString();

        String[] arrSelectData = {strLocalSearch,strLocalSearch,strLocalSearch};
        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectData, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        User userInfo = new User(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }


}
