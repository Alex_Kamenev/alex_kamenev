package com.alexkamenev.database.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.alexkamenev.database.R;
import com.alexkamenev.database.db.DBHelper;
import com.alexkamenev.database.tools_and_constants.AppSettings;
import com.alexkamenev.database.tools_and_constants.DBConstants;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView m_ListView;
    private String[] scope = new String[]{VKScope.MESSAGES, VKScope.FRIENDS};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        VKSdk.login(this, scope);


        View butnToUserActivity = findViewById(R.id.buttonToUserActivity);
        butnToUserActivity.setOnClickListener(this);

        View buttonToCongratulationsActivity = findViewById(R.id.buttonToCongratulationsActivity);
        buttonToCongratulationsActivity.setOnClickListener(this);

        View buttonToSettingActivity = findViewById(R.id.buttonToSettingsActivity);
        buttonToSettingActivity.setOnClickListener(this);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getIsFirstStart()) {
            //Если первый раз - устанавливаем флаг в false, что бы не допустить повторного вызова метода
            appSettings.setIsFirstStart(false);
            //Показываем сообщение на экран
            Toast.makeText(this, "This is first start app!", Toast.LENGTH_LONG).show();
            //Создаем класс-помощника работы с БД
            DBHelper dbHelper = new DBHelper(this);
            //Получаем базу данных для записи
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            //Создаем класс для передачи данных в БД (в формате ключь-значение).
            ContentValues values = new ContentValues();
            //Добавляем данные в объект
            //В качестве ключа используем имена столбцов.
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS, "С днем рождения! Счастья, удачи, лоха побогаче!");
            //Записываем их в БД
            db.insert(DBConstants.TABLE_NAME, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS, "Поздравляю с днем рождения!");
            db.insert(DBConstants.TABLE_NAME, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS, "Happy Birthday!");
            db.insert(DBConstants.TABLE_NAME, null, values);

            //  values.clear();
            //  values.put(DBConstants.TABLE_USERS_FIELD_FIRSTNAME, "Ахмед " );
            //  values.put(DBConstants.TABLE_USERS_FIELD_LASTNAME, "Ажаж");
            //  db.insert(DBConstants.TABLE2_NAME, null, values);

            //После окончания работы с БД - ОБЯЗАТЕЛЬНО ЗАКРЫВАЕМ БД!
            db.close();
            //После закрытия БД - ОБЯЗАТЕЛЬНО ЗАКРЫВАЕМ КЛАСС-ПОМОЩНИК!
            dbHelper.close();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
// Пользователь успешно авторизовался
                m_ListView = (ListView) findViewById(R.id.listViewUserActivity);
                VKRequest request = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, "first_name, last_name"));
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);

                        VKList list = (VKList) response.parsedModel;

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this,
                                android.R.layout.simple_expandable_list_item_1, list);

/                       m_ListView.setAdapter(arrayAdapter);


                    }

                });
            }



            @Override
            public void onError(VKError error) {
// Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonToUserActivity:
                Intent intent2 = new Intent(this, UserActivity.class);
                startActivity(intent2);
                break;
            case R.id.buttonToCongratulationsActivity:
                Intent intent3 = new Intent(this, CongratulationsActivity.class);
                startActivity(intent3);
                break;
            case R.id.buttonToSettingsActivity:
                Intent intent4 = new Intent(this, SettingsActivity.class);
                startActivity(intent4);
                break;
        }
    }
}

