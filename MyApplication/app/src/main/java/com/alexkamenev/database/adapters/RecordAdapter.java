package com.alexkamenev.database.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alexkamenev.database.R;
import com.alexkamenev.database.model.Record;

import java.util.ArrayList;

public class RecordAdapter extends BaseAdapter {

    private ArrayList<Record> m_arrData = null;

    public RecordAdapter(ArrayList<Record> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((Record)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.item_record, parent, false);
        }
        Record info = ((Record)getItem(position));
        TextView tv = (TextView)convertView.findViewById(R.id.gratsTextViewItem);
        tv.setText(info.toString());
        return convertView;
    }
}
