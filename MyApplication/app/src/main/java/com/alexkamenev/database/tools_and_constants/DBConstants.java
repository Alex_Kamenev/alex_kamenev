package com.alexkamenev.database.tools_and_constants;

/**Класс для хранения констант для базы данных.
 * Содержит константы, для работы с БД.
 */
public class DBConstants {

    public static final String TABLE_NAME = "Congratulations";
    public static final String TABLE2_NAME = "Users";

    public static final String TABLE_CONGRATULATIONS_FIELD_ID = "_id";
    public static final String TABLE_CONGRATULATIONS_FIELD_GRATS = "_grats";


    public static final String TABLE_USERS_FIELD_ID = "_id";
    public static final String TABLE_USERS_FIELD_FIRSTNAME = "_firstname";
    public static final String TABLE_USERS_FIELD_LASTNAME = "_lastname";

}
