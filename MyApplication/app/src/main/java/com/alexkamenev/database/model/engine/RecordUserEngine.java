package com.alexkamenev.database.model.engine;

import android.content.Context;

import com.alexkamenev.database.model.User;
import com.alexkamenev.database.model.wrappers.RecordUserWrapper;

import java.util.ArrayList;

public class RecordUserEngine {

    private Context m_context = null;

    public RecordUserEngine(Context context) {
        m_context = context;
    }

    public ArrayList<User> getAll() {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        return wrapper.getAllUsers();
    }

    public User getItemById(long nId) {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        return wrapper.getItemById(nId);
    }

    public void insertItem(User item) {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        wrapper.insertUserItem(item);
    }

    public void updateItem(User item) {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        wrapper.updateUserItem(item);
    }

    public void deleteItem(User item) {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        wrapper.deleteUserItem(item);
    }

    public void deleteAll() {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        wrapper.deleteAll();
    }

    public ArrayList<User> getSearch(String strSearch) {
        RecordUserWrapper wrapper = new RecordUserWrapper(m_context);
        return wrapper.getSearch(strSearch);
    }
}
