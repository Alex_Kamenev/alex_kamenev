package com.alexkamenev.database.model.engine;

import android.content.Context;

import com.alexkamenev.database.model.Record;
import com.alexkamenev.database.model.wrappers.RecordWrapper;

import java.util.ArrayList;

public class RecordEngine {

    private Context m_context = null;

    public RecordEngine(Context context) {
        m_context = context;
    }

    public ArrayList<Record> getAll() {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        return wrapper.getAll();
    }

    public Record getItemById(long nId) {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        return wrapper.getItemById(nId);
    }

    public void insertItem(Record item) {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        wrapper.insertItem(item);
    }

    public void updateItem(Record item) {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        wrapper.updateItem(item);
    }

    public void deleteItem(Record item) {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        wrapper.deleteItem(item);
    }

    public void deleteAll() {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        wrapper.deleteAll();
    }

    public ArrayList<Record> getSearch(String strSearch) {
        RecordWrapper wrapper = new RecordWrapper(m_context);
        return wrapper.getSearch(strSearch);
    }
}
