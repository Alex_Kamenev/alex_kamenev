package com.alexkamenev.database.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.alexkamenev.database.tools_and_constants.DBConstants;

/**
 * Класс реализущий доступ к базе данных
 */
public class DBHelper extends SQLiteOpenHelper {

    /**
     * Конструктор класса. Для открытия БД используется Context.
     * @param context
     */
    public DBHelper(Context context) {
        /*В качестве параметров в конструктор родителя дополнительно передаем
        * имя базы данных (их может быть не одна) и ее (базы данных) версию.*/
        super(context, "db_example", null, 1);
    }

    /**
     * Функция вызываемая системой, в случае обращения к базе данных
     * которой не существует.
     * В данной функции происходит построение базы данных.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.TABLE2_NAME +
                " (" + DBConstants.TABLE_USERS_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                DBConstants.TABLE_USERS_FIELD_FIRSTNAME + " TEXT NOT NULL, "+
                DBConstants.TABLE_USERS_FIELD_LASTNAME + " TEXT NOT NULL); ");

        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME +
                " (" + DBConstants.TABLE_CONGRATULATIONS_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS + " TEXT NOT NULL); ");

    }

    /**
     * Функция вызываемая системой, в случае обращения к базе данных
     * которая устарела.
     * В данной функции происходит обновление базы данных до текущей версии.
     * @param db существующая БД
     * @param oldVersion версия БД которая существует
     * @param currentVersion запращиваемая версия БД
     *
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int currentVersion) {

    }
}
