package com.alexkamenev.database.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alexkamenev.database.R;
import com.alexkamenev.database.model.Record;
import com.alexkamenev.database.model.engine.RecordEngine;

public class ComposeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_KEY_ID = "EXTRA_KEY_ID";

    /*private long m_nId = -1;*/
    private Record m_congratulations = null;

    private EditText m_gratsEditText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        Button btnAdd = (Button) findViewById(R.id.addButtonComposeActivity);
        btnAdd.setOnClickListener(this);

        Button btnRemove = (Button) findViewById(R.id.removeButtonComposeActivity);
        btnRemove.setOnClickListener(this);

        m_gratsEditText = (EditText) findViewById(R.id.gratsEditTextComposeActivity);



        Intent intent = getIntent();
        long nId = -1;
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nId = bundle.getLong(EXTRA_KEY_ID,-1);
            }
        }

        if (nId!=-1){
            btnAdd.setText("Обновить");

            btnRemove.setVisibility(View.VISIBLE);

            RecordEngine engine = new RecordEngine(this);
            m_congratulations = engine.getItemById(nId);
            m_gratsEditText.setText(m_congratulations.getGrats());
        }
    }

    @Override
    public void onClick(View v) {
        RecordEngine engine = new RecordEngine(this);
        switch (v.getId()){
            case R.id.addButtonComposeActivity:
                if (m_congratulations ==null){
                    m_congratulations = new Record(m_gratsEditText.getText().toString(),
                            m_gratsEditText.getText().toString(),
                            m_gratsEditText.getText().toString());
                } else {
                    m_congratulations.setGrats(m_gratsEditText.getText().toString());

                }

                if(m_congratulations.getId()!=-1){
                    engine.updateItem(m_congratulations);
                } else {
                    engine.insertItem(m_congratulations);
                }
                break;

            case R.id.removeButtonComposeActivity:
                engine.deleteItem(m_congratulations);
                break;
        }
        finish();
    }
}
