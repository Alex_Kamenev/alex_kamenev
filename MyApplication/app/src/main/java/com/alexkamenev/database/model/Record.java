package com.alexkamenev.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.alexkamenev.database.activities.ComposeActivity;
import com.alexkamenev.database.tools_and_constants.DBConstants;

public class Record {

    private long m_nId = -1;
    private String m_strGrats = "";


    public Record(String m_strGrats, String m_strSName, String m_strPhone) {
        this.m_nId = -1;
        this.m_strGrats = m_strGrats;

    }

    public Record(Cursor cursor){
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_CONGRATULATIONS_FIELD_ID));
        this.m_strGrats = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS));
    }




    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getGrats() {
        return m_strGrats;
    }

    public void setGrats(String m_strName) {
        this.m_strGrats = m_strName;
    }


    @Override
    public String toString() {
        return "" + m_strGrats + "\n";
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS, m_strGrats);

        return values;
    }
}
