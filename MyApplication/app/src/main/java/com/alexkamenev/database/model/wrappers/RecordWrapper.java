package com.alexkamenev.database.model.wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alexkamenev.database.model.Record;
import com.alexkamenev.database.tools_and_constants.DBConstants;

import java.util.ArrayList;


public class RecordWrapper extends BaseDBWrapper {

    public RecordWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME);
    }

    public ArrayList<Record> getAll(){
        ArrayList<Record> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        Record userInfo = new Record(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public Record getItemById(long nId){
        Record userInfo = null;
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_CONGRATULATIONS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    userInfo = new Record(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return userInfo;
    }

    public void insertItem(Record item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        db.insert(getTableName(),null,values);
        db.close();
    }

    public void updateItem(Record item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        String strSelection = DBConstants.TABLE_CONGRATULATIONS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.update(getTableName(),values,strSelection, arrSelectionArgs);
        db.close();
    }

    public void deleteItem(Record item){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_CONGRATULATIONS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }

    public ArrayList<Record> getSearch(String strSearch){
        String strLocalSearch = "%" + strSearch + "%";
        ArrayList<Record> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        StringBuilder selectionBuilder = new StringBuilder();
        selectionBuilder.append(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS )
                .append(" LIKE ? OR ")
                .append(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS)
                .append(" LIKE ? OR ")
                .append(DBConstants.TABLE_CONGRATULATIONS_FIELD_GRATS)
                .append(" LIKE ?");
        String strSelection = selectionBuilder.toString();

        String[] arrSelectData = {strLocalSearch,strLocalSearch,strLocalSearch};
        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectData, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        Record userInfo = new Record(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }


}
