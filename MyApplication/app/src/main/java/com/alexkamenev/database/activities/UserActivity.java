package com.alexkamenev.database.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.alexkamenev.database.R;
import com.alexkamenev.database.adapters.RecordUserAdapter;
import com.alexkamenev.database.model.User;
import com.alexkamenev.database.model.engine.RecordUserEngine;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKList;

import java.util.ArrayList;

import static com.alexkamenev.database.activities.ComposeActivity.EXTRA_KEY_ID;

public class UserActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView m_ListView = null;
    private ArrayList<User> m_arrUserData = new ArrayList<>();
    private RecordUserAdapter m_userAdapter = null;
    private String[] scope = new String[]{VKScope.MESSAGES, VKScope.FRIENDS};
    private String m_strSearchString = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_layout);


        EditText editText = (EditText) findViewById(R.id.SearchEditTextUserActivity);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSearchString = s.toString();
                updateList();

            }
        });

        Intent intent = getIntent();
        long nId = -1;
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                nId = bundle.getLong(EXTRA_KEY_ID, -1);
            }
        }


        m_userAdapter = new RecordUserAdapter(m_arrUserData);
//        m_ListView.setAdapter(m_userAdapter);
        //       m_ListView.setOnItemClickListener(this);

        View btnAdd = findViewById(R.id.addButtonUserActivity);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.removeAllButtonUserActivity);
        btnRemoveAll.setOnClickListener(this);

    }


        @Override
        protected void onActivityResult ( int requestCode, int resultCode, Intent data) {
            if (VKSdk.isLoggedIn()) {
                if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
                    @Override
                    public void onResult(VKAccessToken res) {
// Пользователь успешно авторизовался
                        m_ListView = (ListView) findViewById(R.id.listViewUserActivity);
                        VKRequest request = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, "first_name, last_name"));
                        request.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                super.onComplete(response);

                                VKList list = (VKList) response.parsedModel;

                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UserActivity.this,
                                        android.R.layout.simple_expandable_list_item_1, list);

                                m_ListView.setAdapter(arrayAdapter);


                            }

                        });
                    }

                    @Override
                    public void onError(VKError error) {
// Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                    }
                })) {
                    super.onActivityResult(requestCode, resultCode, data);
                }
            }
        }

            @Override
            protected void onResume () {
                super.onResume();
                updateList();
            }


    private void updateList() {

        RecordUserEngine engine = new RecordUserEngine(this);
        ArrayList<User> arrUserData = engine.getAll();

        if (m_strSearchString.isEmpty()) {
            arrUserData = engine.getAll();
        } else {
            arrUserData = engine.getSearch(m_strSearchString);
        }

        m_arrUserData.clear();
        m_arrUserData.addAll(arrUserData);
        m_userAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.removeAllButtonUserActivity:
                RecordUserEngine engine = new RecordUserEngine(this);
                engine.deleteAll();
                updateList();
                break;
            case R.id.addButtonUserActivity:

                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}

