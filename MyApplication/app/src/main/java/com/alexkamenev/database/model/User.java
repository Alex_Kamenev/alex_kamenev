package com.alexkamenev.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.alexkamenev.database.tools_and_constants.DBConstants;

public class User {

    private long userId = -1;
    private String m_strFirstName = "";
    private String m_strLastName = "";



    public User(long m_nId, String m_strFirstName, String m_strLastName) {

        this.userId = -1;
        this.m_strFirstName = "";
        this.m_strLastName = "";

    }


    public User(Cursor cursor){
        this.userId = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_USERS_FIELD_ID));
        this.m_strFirstName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_USERS_FIELD_FIRSTNAME));
        this.m_strLastName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_USERS_FIELD_LASTNAME));
    }



    public long getId() {
        return userId;
    }

    public void setId(long m_nId) {
        this.userId = m_nId;
    }

    public String getFirstName() {
        return m_strFirstName;
    }

    public void setFirstName(String m_strFirstName) {
        this.m_strFirstName = m_strFirstName;
    }

    public String getLastName() {
        return m_strLastName;
    }

    public void setLastName(String m_strLastName) { this.m_strLastName = m_strLastName; }


    @Override
    public String toString() {
        return "" + m_strLastName + "" + m_strFirstName + "\n";
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.TABLE_USERS_FIELD_FIRSTNAME, m_strFirstName);
        values.put(DBConstants.TABLE_USERS_FIELD_LASTNAME, m_strLastName);

        return values;
    }
}
