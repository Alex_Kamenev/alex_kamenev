package com.alexkamenev.database.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alexkamenev.database.R;
import com.alexkamenev.database.model.User;

import java.util.ArrayList;

public class RecordUserAdapter extends BaseAdapter {

    private ArrayList<User> m_arrData = null;

    public RecordUserAdapter(ArrayList<User> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((User)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.user_record, parent, false);
        }
        User info = ((User)getItem(position));
        TextView tv = (TextView)convertView.findViewById(R.id.userTextViewItem);
        tv.setText(info.toString());
        return convertView;
    }
}
