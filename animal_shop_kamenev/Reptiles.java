package animal_shop_kamenev;

import java.util.Scanner;

public abstract class Reptiles extends Animals {
    //unique parameter for Reptiles
    private String m_strPoisonous = "";

    public void setPoisonous(String strPoisonous) {

        m_strPoisonous = strPoisonous;

    }

    public String getPoisonous() {

        return m_strPoisonous;

    }

}
