package animal_shop_kamenev;

public class Bugs_not_abstract extends Bugs {

    Bugs_not_abstract() { //constuctor

    }

    @Override
    public void dataInput() { //input of all data about a Bug

        System.out.println("Enter name; ");
        String strName = scan.nextLine();
        System.out.println("Enter color: ");
        String strColor = scan.nextLine();
        System.out.println("Enter weight: ");
        float fWeight = scan.nextFloat();
        System.out.println("How manu legs?");
        int nLegsCount = scan.nextInt();

        setName(strName);
        setColor(strColor);
        setWeight(fWeight);
        setLegsCount(nLegsCount);

    }

    @Override //output
    public void dataOutput() {
        System.out.println("Kind: Bug.\n"
                + "The name of your animal is " + getName() + ".\n"
                + "Color of your animal is" + getColor() + "."
                + "Your animals weights" + getWeight() + "kilo.\n"
                + "And it has " + getLegsCount() + "legs.\n");
    }

    @Override //search
    public boolean isSearch(String strSearch) {
        boolean bResult = false;
        bResult = getColor().equalsIgnoreCase(strSearch)
                || getName().toLowerCase().contains(strSearch.toLowerCase());
        return bResult;
    }
}
