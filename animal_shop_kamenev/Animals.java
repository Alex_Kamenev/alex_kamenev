package animal_shop_kamenev;

public abstract class Animals {

   
    private String m_strName = "";//name of animal
    private String m_strColor = "";
    float m_fWeight = 0;

    //setters
    public void setName(String strName) {

        m_strName = strName;
    }

    public void setColor(String strColor) {

        m_strColor = strColor;

    }

    public void setWeight(float fWeight) {

        m_fWeight = fWeight;

    }

    
    //getters
    public String getName() {

        return m_strName;
    }

    public String getColor() {

        return m_strColor;

    }

    public float getWeight() {

        return m_fWeight;

    }
    //functions that are used in next classes
    public abstract void dataInput();

    public abstract void dataOutput();

    public abstract boolean isSearch(String strSearch);
}
