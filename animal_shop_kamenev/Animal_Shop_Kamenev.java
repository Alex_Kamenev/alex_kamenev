package animal_shop_kamenev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Animal_Shop_Kamenev {

    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in); //input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));//input
        ArrayList<Animals> arrData = new ArrayList<>();//creating new ArrayList
        //menu
        do {
            System.out.println("Menu:");
            System.out.println("1. Add animal");
            System.out.println("2. Show list");
            System.out.println("3. Search");
            System.out.println("4. Exit");
            int nVal = Integer.valueOf(br.readLine());//using BufferedReader
            switch (nVal) {
                case 1://choosing an animal
                    System.out.println("Sub-menu:");
                    System.out.println("1. Reptile");
                    System.out.println("2. Bug");
                    nVal = Integer.valueOf(br.readLine());
                    Animals animals = null;
                    switch (nVal) {
                        case 1://Snakes and Toads are reptiles
                            System.out.println("Reptiles:");
                            System.out.println("1. Snake");
                            System.out.println("2. Toad");
                            nVal = Integer.valueOf(br.readLine());

                            switch (nVal) {
                                case 1://snakes
                                    animals = new Snakes();
                                    break;

                                case 2://toads
                                    animals = new Toads();
                                    break;

                            }
                            break;
                        case 2://bugs
                            animals = new Bugs_not_abstract();
                            break;
                    }
                    if (animals != null) {
                        animals.dataInput();
                        arrData.add(animals);//adding to ArrayList
                    }
                    break;
                case 2://showing list of animal in ArrayList
                    for (Animals animalsForOutput : arrData) {
                        animalsForOutput.dataOutput();
                    }
                    break;
                case 3: //search
                    scan.nextLine();
                    String strSearch = scan.nextLine();
                    for (Animals animalsForSearch : arrData) {
                        if (animalsForSearch.isSearch(strSearch)) {
                            animalsForSearch.dataOutput();
                        }
                    }
                    break;
                case 4://exit
                    return;
            }
        } while (true);

    }

}
