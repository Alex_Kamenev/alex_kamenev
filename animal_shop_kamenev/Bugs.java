package animal_shop_kamenev;

import java.util.Scanner;

public abstract class Bugs extends Animals {
    //unique parameter for Bugs
    Scanner scan = new Scanner(System.in);

    private int m_nLegsCount = 0;

    public void setLegsCount(int nLegsCount) {

        m_nLegsCount = nLegsCount;

    }

    public int getLegsCount() {

        return m_nLegsCount;

    }

}
