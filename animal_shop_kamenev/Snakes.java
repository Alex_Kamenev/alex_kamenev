package animal_shop_kamenev;

import java.util.Scanner;

public class Snakes extends Reptiles {

    @Override
    public void dataInput() {//input of all data about a Snake

        Scanner scan = new Scanner(System.in);

        System.out.println("Enter name; ");
        String strName = scan.nextLine();
        System.out.println("Enter color: ");
        String strColor = scan.nextLine();
        System.out.println("Enter weight: ");
        float fWeight = scan.nextFloat();
        System.out.println("Poisonous or not?");
        String strPoisonous1 = scan.nextLine();
        String strPoisonous = scan.nextLine();

        setName(strName);
        setColor(strColor);
        setWeight(fWeight);
        setPoisonous(strPoisonous);
    }

    @Override
    public void dataOutput() {
        System.out.println("Kind: Snake" + ".\n"
                + "The name of your animal is " + getName() + ".\n"
                + "Color of your animal is " + getColor() + ".\n"
                + "Your animals weights " + getWeight() + " kilo.\n"
                + "And it's " + getPoisonous() + ".");

    }

    @Override
    public boolean isSearch(String strSearch) {
        boolean bResult = false;
        bResult = getColor().equalsIgnoreCase(strSearch)
                || getName().toLowerCase().contains(strSearch.toLowerCase());
        return bResult;
    }

}
