
package triangle_kamenev;

import java.util.Scanner;
import java.lang.Math; //is used to calculate square
public class Triangle_Kamenev {

    
    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
         
        Triangle triangle = null; 
        triangle = new Triangle();
        
        do{ //MENU
            System.out.println("Menu: ");
            System.out.println("0. New triangle.");
            System.out.println("1. Input first side.");
            System.out.println("2. Input second side.");
            System.out.println("3. Input third side.");
            System.out.println("4. Get square.");
            System.out.println("5. Get perimetr.");
            System.out.println("6. Is equilateral.");
            System.out.println("7. Exit.");
            int nTypeValue = scanner.nextInt();
            
            if (nTypeValue==7){ //exits program if value is 7
                break;
            }
            switch(nTypeValue){
                case 0:
                    triangle = new Triangle(); //creating a new triangle
                    break;
                case 1: //entering first side 
                    float fSide1 = 0;
                    do{
                        System.out.print("Enter first side: ");
                        fSide1 = scanner.nextFloat();
                    } while(!triangle.setSide1(fSide1));                    
                    break;
                case 2://entering second side 
                    float fSide2 = 0;
                    do{
                        System.out.print("Enter second side: ");
                        fSide2 = scanner.nextFloat();
                    } while(!triangle.setSide2(fSide2));                    
                    break;
                case 3://entering third side 
                    float fSide3 = 0;
                    do{
                        System.out.print("Enter third side: ");
                        fSide3 = scanner.nextFloat();
                    } while(!triangle.setSide3(fSide3));                    
                    break;
                case 4: // value 4 is for Square
                    if (triangle.isCorrectInput()){
                        System.out.println("Square: " + triangle.getSquare());
                    } else {
                        System.err.println("Incorrect data!!!!");
                    }
                    break;                  
                case 5: // value 5 is for Perimetr
                    if (triangle.isCorrectInput()){
                        System.out.println("Perimetr: " + triangle.getPerimetr());
                    } else {
                        System.err.println("Incorrect data!!!!");
                    }
                    break;
                case 6: //checking input
                    if (triangle.isCorrectInput()){
                        System.out.println("Is equilateral: " + triangle.isEquilateral());
                    } else {
                        System.err.println("Incorrect data!!!!");
                    }
                    break;
                default:
                    System.err.println("Incorrect input!!!!");
                    break;
            }
        } while(true);
    
        System.out.println();
 
    }
}
