
package triangle_kamenev;
import java.lang.Math;

public class Triangle {
    
    private double m_fSide1 = 0;
    private double m_fSide2 = 0;
    private double m_fSide3 = 0;
    
public Triangle(){ //new triangle
       
    m_fSide1 = 0;
    m_fSide2 = 0;
    m_fSide3 = 0;
}

public Triangle(double fVal){//new triangle with value
       
    m_fSide1 = fVal;
    m_fSide2 = fVal;
    m_fSide3 = fVal;
}

public double getPerimetr(){// perimetr of triangle is a+b+c
        double fResurs = m_fSide1+m_fSide2+m_fSide3;
        return fResurs;
    }

public boolean isEquilateral(){// check if triangle is equilateral
        boolean bResult = (m_fSide1==m_fSide2)&&(m_fSide2==m_fSide3)&&(m_fSide1==m_fSide3);
        return bResult;
    }
public boolean isCorrectInput(){ //checking input
        boolean bResult = (m_fSide1>0)&&(m_fSide2>0)&&(m_fSide3>0);
        return bResult;
    }
public double getSquare(){ //Heron's formula S=square root of p*(p-a)*(p-b)*(p-c)
    double semiPerimetr = (m_fSide1+m_fSide2+m_fSide3)/2;
       double square1 = semiPerimetr*(semiPerimetr - m_fSide1)*(semiPerimetr - m_fSide2)*(semiPerimetr - m_fSide3);
       double squareResult = Math.sqrt(square1);
       return squareResult;
       
    }
//setters
public boolean setSide1(double fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide1 = fVal;
            bResult = true;
        }
        return bResult;
    }
    
public boolean setSide2(double fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide2 = fVal;
            bResult = true;
        }
        return bResult;
    }
    
public boolean setSide3(double fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide3 = fVal;
            bResult = true;
        }
        return bResult;
    }
//getters
public double getSide1(){
        return m_fSide1;
    }
    
public double getSide2(){
        return m_fSide2;
    }
    
public double getSide3(){
        return m_fSide3;
    }

}