package task4_kamenev;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Task4_Kamenev {
   

    public static void main(String[] args) {
        ArrData arr = new ArrData();//now we can use class ArrData.java
        Scanner scan = new Scanner(System.in);//input
        
        Float fVal = new Float(0);//adding elements to our List
        do{
            System.out.println("Enter positive number:");
            fVal = scan.nextFloat();
            arr.f(fVal);
          
        } while (fVal>0);//input wiil stop when negative number is added
        arr.arrData.remove(fVal);
        
        System.out.println("Before sort" + arr.arrData);//print List before it's sorted
        List<Float> arrData2 = new ArrayList<Float>(arr.arrData);//shallow copy
       //sort
        Collections.sort(arrData2, new Comparator<Float>() {
        public int compare(Float o1, Float o2) {
                return o1.compareTo(o2);
        }
        });
        
        System.out.println("After sort: ");//print our first List
        System.out.println(arrData2);//sorted List
        
        float fAverage = 0;//calculating the average
        
        for (Float fValForfAverage:arrData2){
            fAverage += fValForfAverage/arrData2.size();
        }
        
         System.out.println("Average: " + fAverage);//printing average
         
         System.out.println("Before sort" + arr.arrData);
         
         System.out.println("Elements before sort that not less than average: ");
         for(Float f:arr.arrData){//printing elements that are not less than average
         
            if(f >= fAverage){
         
            
             System.out.println(f);
         
         
            }
         
        }
         
    }
}
