package com.alexkamenev.vkbirthdays.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.alexkamenev.vkbirthdays.R;
import com.alexkamenev.vkbirthdays.db.DBHelper;
import com.alexkamenev.vkbirthdays.fragments.FragmentAbout;
import com.alexkamenev.vkbirthdays.fragments.FragmentCongratulations;
import com.alexkamenev.vkbirthdays.fragments.FragmentFriends;
import com.alexkamenev.vkbirthdays.tools_and_constants.AppSettings;
import com.alexkamenev.vkbirthdays.tools_and_constants.DBConstants;
import com.crashlytics.android.Crashlytics;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private boolean isResumed = false;
    static MainActivity mainActivity;
    int smile = 0x1F60A;
    int cake = 0x1F382;
    int cap = 0x1F389;
    int kiss = 0x1F61A;
    int kiss_with_heart = 0x1F618;
    int flowers = 0x1F490;
    int watermelon = 0x1F349;
    int teeth = 0x1F62C;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;


    public void showLogin() {
        startActivityForResult(new Intent(this, LoginInActivity.class), 1);
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, final Intent data) {

        new Thread(new Runnable() {
            @Override
            public void run() {


                VKRequest request = VKApi.friends().get(VKParameters.from(
                        VKApiConst.FIELDS, "photo_100,online,bdate",
                        VKApiConst.NAME_CASE, "nom",
                        "order", "hints"));

                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                        try {
                            parseJson(response.json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                if (requestCode == 1) {
                    if (data == null) {
                        return;
                    }
                    String backpressed = data.getStringExtra("backpressed");
                    if (backpressed.equals("1")) finish();
                }
            }
        }).run();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
        if (!VKSdk.isLoggedIn()) {
            showLogin();
        }
    }

    @Override
    protected void onPause() {
        isResumed = false;
        super.onPause();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fabric.with(this, new Crashlytics());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("VK Поздравления");


    /*    View buttonToUserActivity = findViewById(R.id.button_friends);
        buttonToUserActivity.setOnClickListener(this);

        View buttonToCongratulationsActivity = findViewById(R.id.button_congratulations);
        buttonToCongratulationsActivity.setOnClickListener(this);

        View buttonToSettingActivity = findViewById(R.id.button_settings);
        buttonToSettingActivity.setOnClickListener(this); */

        mainActivity = this;

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getIsFirstStart()) {

            //Если первый раз - устанавливаем флаг в false, что бы не допустить повторного вызова метода
            appSettings.setIsFirstStart(false);

            //Создаем класс-помощника работы с БД

            DBHelper dbHelper = new DBHelper(this);
            //Получаем базу данных для записи
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            //Создаем класс для передачи данных в БД (в формате ключь-значение).
            ContentValues values = new ContentValues();
            //Добавляем данные в объект
            //В качестве ключа используем имена столбцов.
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения!" + getEmijoByUnicode(cake) +
                    "\nВсего тебя самого яркого от этой жизни" + getEmijoByUnicode(smile));
            //Записываем их в БД
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения!\n" +
                    "Желаю счастья и море позитива всегда)" + getEmijoByUnicode(kiss) + getEmijoByUnicode(kiss));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения тебя!" + getEmijoByUnicode(kiss_with_heart));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения) \n" +
                    "Всего самого хорошего и прекрасного) получай от жизни всё!\n"
                    + getEmijoByUnicode(smile) + getEmijoByUnicode(teeth) + getEmijoByUnicode(teeth));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения!\n"
                    + "Желаю тебе в этой жизни всего самого яркого и сочного!" + getEmijoByUnicode(watermelon));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения :)\n"
                    + "Пусть твоя жизнь всегда будет наполнена приятными моментами, как сегодня" + getEmijoByUnicode(smile) + getEmijoByUnicode(cake));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "Поздравляю тебя с Днем Рождения!\n Желаю многооооо счастья, крепкого здоровья)) " +
                    "А еще побольше приятных моментов в жизни, удачи и везения, верных друзей и приятных сюрпризов, а так же веселой жизни))\n" +
                    " пусть все поставленные тобой цели будут достигнуты, а мечты постепенно сбываются" +
                    getEmijoByUnicode(cake) + getEmijoByUnicode(cap) + getEmijoByUnicode(cap));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С днем рождения! " + getEmijoByUnicode(kiss_with_heart) +
                    "\nЖелаю тебе всего самого яркого в жизни, по-больше приятных событий, личностного роста и верных друзей всегда рядом!" + getEmijoByUnicode(smile));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);

            values.clear();
            values.put(DBConstants.TABLE_CONGRATULATIONS_FIELD_CONGRATULATIONS, "С Днем рождения тебя!\n" + "Улыбайся, добивайся своих целей. " +
                    "Желаю тебе успешной самореализации и дальше по жизни, кайфуй!\n" +
                    "Реально кайфуй от того,что ты делаешь!" + getEmijoByUnicode(flowers) + getEmijoByUnicode(cake));
            db.insert(DBConstants.TABLE_CONGRATULATIONS, null, values);


            //После окончания работы с БД - ОБЯЗАТЕЛЬНО ЗАКРЫВАЕМ БД!
            db.close();
            //После закрытия БД - ОБЯЗАТЕЛЬНО ЗАКРЫВАЕМ КЛАСС-ПОМОЩНИК!
            dbHelper.close();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                VKSdk.logout();
                startLoginActivity();
                break;
            case R.id.action_banned:
                Intent intent1 = new Intent(this, BannedFriends.class);
                startActivity(intent1);

//                String firstname = "";
//                VKRequest reques1 = new VKRequest("account.getProfileInfo", VKParameters.from( VKApiConst.FIELDS,"first_name"));
//                reques1.executeWithListener(new VKRequest.VKRequestListener() {
//                    @Override
//                    public void onComplete(VKResponse response) {
//                        super.onComplete(response);
//                        try {
//                            parseMyName(response.json);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                });

                break;
            case R.id.setTime:
                Intent intent2 = new Intent(this, AlarmActivity.class);
                startActivity(intent2);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void startLoginActivity() {
        startActivity(new Intent(this, LoginInActivity.class));
        finish();
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentFriends(), "Друзья");
        adapter.addFragment(new FragmentCongratulations(), "Поздравления");
        adapter.addFragment(new FragmentAbout(), "О приложении");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public String getEmijoByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    private void parseJson(JSONObject json) throws JSONException {

        try {
            JSONArray allFriends = json.getJSONObject("response").getJSONArray("items");
            for (int i = 0; i < allFriends.length(); i++) {
                JSONObject friend;
                friend = allFriends.getJSONObject(i);
                int id = friend.getInt("id");
                String name = friend.getString("first_name");
                String lastname = friend.getString("last_name");

                String bdate = friend.optString("bdate", null);

                if (bdate == null) {
                    continue;
                }


                SimpleDateFormat curFormater = new SimpleDateFormat("dd.MM", Locale.ENGLISH);
                Date dateObj = curFormater.parse(bdate);
                SimpleDateFormat postFormater = new SimpleDateFormat("d MMMM");

                String newDateStr = postFormater.format(dateObj);

                bdate = newDateStr;


                DBHelper dbHelper = new DBHelper(this);

                SQLiteDatabase db = dbHelper.getWritableDatabase();

                String strSelect = DBConstants.TABLE_FRIENDS_FIELD_USER_ID + "=?";
                String[] arrAgg = new String[]{Integer.toString(id)};
                Cursor cursor = db.query(DBConstants.TABLE_FRIENDS, null, strSelect, arrAgg, null, null, null);

                if (cursor != null) {
                    try {
                        if (cursor.getCount() == 1) {
                            continue;
                        }
                    } finally {
                        cursor.close();
                    }
                }


                ContentValues values = new ContentValues();

                values.put(DBConstants.TABLE_FRIENDS_FIELD_NAME, name);
                values.put(DBConstants.TABLE_FRIENDS_FIELD_SNAME, lastname);
                values.put(DBConstants.TABLE_FRIENDS_FIELD_USER_ID, id);
                values.put(DBConstants.TABLE_FRIENDS_FIELD_BDATE, bdate);


                db.insert(DBConstants.TABLE_FRIENDS, null, values);

                db.close();

                dbHelper.close();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

//    public void parseMyName(JSONObject json) throws JSONException {
//        try {
//            JSONArray myacc = json.getJSONObject("response").getJSONArray("items");
//
//            JSONObject me;
//            me = myacc.getJSONObject(0);
//            String name = me.getString("first_name");
//            Toast.makeText(MainActivity.this, name, Toast.LENGTH_LONG).show();
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

}