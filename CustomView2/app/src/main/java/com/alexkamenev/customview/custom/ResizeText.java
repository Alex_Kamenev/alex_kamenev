package com.alexkamenev.customview.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexkamenev.customview.R;

/**
 * Created by hug3m on 19.09.2016.
 */
public class ResizeText extends RelativeLayout {

    private TextView m_tv = null;
    private TextView m_tv2 = null;
    private TextView m_tv3 = null;
    private boolean m_bIsStartCalc = true;


    public ResizeText(Context context) {
        super(context);
    }
    public ResizeText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ResizeText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.resize_text_layout, this, true);
        m_tv = (TextView) findViewById(R.id.tv_1);
        m_tv2 = (TextView) findViewById(R.id.tv_2);
        m_tv3 = (TextView) findViewById(R.id.tv_3);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    if (m_bIsStartCalc){
        m_bIsStartCalc = true;
        int nRealWidth = MeasureSpec.getSize(widthMeasureSpec);
        int nRealHeight = MeasureSpec.getSize(heightMeasureSpec);
        m_tv.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        m_tv2.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        m_tv3.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int nTextViewWidth1 = m_tv.getMeasuredWidth();
        int nTextViewWidth2 = m_tv2.getMeasuredWidth();
        int nTextViewWidth3 = m_tv3.getMeasuredWidth();

        float fDelta1 = (float)nRealWidth/nTextViewWidth1;
        float fDelta2 = (float)nRealWidth/nTextViewWidth2;
        float fDelta3 = (float)nRealWidth/nTextViewWidth3;


        float fTextSize1 = m_tv.getTextSize() * fDelta1;
        float fTextSize2 = m_tv.getTextSize() * fDelta2;
        float fTextSize3 = m_tv.getTextSize() * fDelta3;

        m_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize1);
        m_tv2.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize2);
        m_tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize3);
        while((m_tv.getMeasuredWidth()>nRealWidth)&&(m_tv2.getMeasuredWidth()>nRealWidth)&&
                (m_tv3.getMeasuredWidth()>nRealWidth)){
            fTextSize1-=1;
            fTextSize2-=1;
            fTextSize3-=1;

            m_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize1);
            m_tv2.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize2);
            m_tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize3);
            m_tv.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            m_tv2.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            m_tv3.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        }


       }
    }
}
