package com.alexkamenev.jailtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements OnClickListener {

    Button btnActTwo;
    private EditText m_EditNameText = null;
    private final int REQUEST_CODE_SHOW_INFO = 235;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_EditNameText = (EditText) findViewById(R.id.EditTextName);

        btnActTwo = (Button) findViewById(R.id.FirstScreenButton);
        btnActTwo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String strName = m_EditNameText.getText().toString();
        Intent intent = new Intent(MainActivity.this, ActivityTwo.class);
        intent.putExtra(ActivityTwo.EXTRA_KEY_STRING_NAME, strName);
        startActivityForResult(intent, REQUEST_CODE_SHOW_INFO);

        switch (v.getId()) {
            case R.id.FirstScreenButton:
                // TODO Call second activity

                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("alex", "onActivityResult -> requestCode ->" + requestCode);
        if (requestCode == REQUEST_CODE_SHOW_INFO) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        String strText = bundle.getString(ActivityTwo.EXTRA_KEY_STRING_TEXT, "");
                        Log.d("devcpp", "onActivityResult -> data -> " + strText);
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("devcpp", "onActivityResult -> resultCode -> RESULT_CANCELED");
            }

        }
    }

}