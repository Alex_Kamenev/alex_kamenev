package com.alexkamenev.jailtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class ActivityThree extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);
    }

    static final private int CHOOSE_ANSWER = 0;

    public void onClick(View v) {
        Intent questionIntent = new Intent(ActivityThree.this,
                ActivityTwo.class);
        startActivityForResult(questionIntent, CHOOSE_ANSWER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TextView answerTextView = (TextView) findViewById(R.id.ThirdTextView);

        if (requestCode == CHOOSE_ANSWER) {
            if (resultCode == RESULT_OK) {
                String answer = data.getStringExtra(ActivityTwo.ANSWER);
                answerTextView.setText(answer);
            } else {
                answerTextView.setText(""); // стираем текст
            }
        }
    }
}