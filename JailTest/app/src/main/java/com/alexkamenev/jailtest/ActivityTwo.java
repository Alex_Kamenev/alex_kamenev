package com.alexkamenev.jailtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ActivityTwo extends Activity {
    Button btnActThree;
    public static final String EXTRA_KEY_STRING_NAME = "EXTRA_KEY_STRING_NAME";
    public static final String EXTRA_KEY_STRING_TEXT = "EXTRA_KEY_STRING_TEXT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        String strName = "";
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                strName = bundle.getString(EXTRA_KEY_STRING_NAME, "");
            }
        }

        TextView nameTextView = (TextView) findViewById(R.id.NameTextView);
        nameTextView.setText(strName + " ");


        btnActThree = (Button) findViewById(R.id.SecondScreenButton);
        btnActThree.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.SecondScreenButton:
                        // TODO Call second activity
                        Intent intent = new Intent(ActivityTwo.this, ActivityThree.class);
                        startActivity(intent);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public final static String ANSWER = "com.alexkamenev.jailtest.Answer";

    public void onRadioClick(View v) {
        Intent answerIntent = new Intent();

        switch (v.getId()) {
            case R.id.RadioButtonBread:
                answerIntent.putExtra(ANSWER, "Хлеб с параши");
                break;
            case R.id.RadioButtonSoap:
                answerIntent.putExtra(ANSWER, "Мыло со стола");
                break;


            default:
                break;
        }

        setResult(RESULT_OK, answerIntent);
        finish();
    }

}