
package task2_2_kamenev;

public class Factorial
{
 public static long factorial(long x)
 {
 if (x<0) throw new IllegalArgumentException ("x должен быть >=0"); // исключительная ситуация
 if (x<=1) return 1;    // здесь рекурсия прекращается
 else return x*factorial(x-1);  // рекурсия - метод вызывает сам себя
 }

}