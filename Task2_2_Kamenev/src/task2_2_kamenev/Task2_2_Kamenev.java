/*Считывает с клавиатуры число. Если число не положительное – выводит его в 10 степени,
а если положительное – его факториал.*/
package task2_2_kamenev;
import java.util.Scanner;

public class Task2_2_Kamenev {
  
//we use method pow to calculate the degree of a number
    public static int pow(int nVal, int nPow){
//there are two arguments: first is number we enter, second is a degree
     int nResult = -1;
    
     if(nVal<0){
     if(nPow>=0){
        
     if(nPow==0){
         nResult = 1;}//recursion stops
     else {
         nResult = nVal*pow(nVal,nPow-1);
            }//recursion starts
        
        }
     }
     return nResult;
}
//we use method factorial to calculate a factorial of a number
    public static int factorial(int x)
 {
 
 if (x<=1) return 1; //recursion stops
 else return x*factorial(x-1);  //recursion starts
 }

    public static void main(String[] args) {
        //scanner is used to input data
        Scanner scan = new Scanner(System.in);
       
       int nRes1 = 0;
       int nVal1 = 0;
       System.out.print("Enter a number: ");
       nVal1 = scan.nextInt();
       
       if (nVal1 < 0){ //if number is negative pow calculates its degree
        nRes1 = pow(nVal1,10);
        }
       else{        //if number is positive factorial caltulates a factorial
           nRes1 = factorial(nVal1);
       }
       
       System.out.println("Result: " + nRes1);
    }
    
}
