package com.alexkamenev.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button firstButton;
    EditText editTextName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = (EditText) findViewById(R.id.editText_name);
        firstButton = (Button) findViewById(R.id.button_first_screen);
        firstButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_first_screen:
                Intent intent = new Intent(this, ActivityTwo.class);
                intent.putExtra("name", editTextName.getText().toString());
                startActivity(intent);

                break;
            default:
                break;
        }

    }

    public void afterTextChanged(Editable s){



    }
}