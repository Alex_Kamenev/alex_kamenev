package com.alexkamenev.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityThree extends AppCompatActivity {
    TextView qname;
    TextView red;
    TextView blue;
    TextView yellow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);
        qname = (TextView) findViewById(R.id.name);
        red = (TextView) findViewById(R.id.tv_red);
        yellow = (TextView) findViewById(R.id.tv_yellow);
        blue = (TextView) findViewById(R.id.tv_blue);

        Intent intent = getIntent();

        String editTextName = intent.getStringExtra("name");
        String s_yellow = intent.getStringExtra("color");
        String s_blue = intent.getStringExtra("color");
        String s_red = intent.getStringExtra("color");

        qname.setText(editTextName + "" + " и вы выбрали");

        yellow.setText(s_yellow);
    }
}