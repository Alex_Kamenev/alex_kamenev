package com.alexkamenev.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityTwo extends AppCompatActivity implements View.OnClickListener {
    TextView tv_Name;
    Button button_yellow;
    Button button_blue;
    Button button_red;
    String yellow;
    String blue;
    String red;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
        tv_Name = (TextView) findViewById(R.id.tv_Name);
        button_blue = (Button) findViewById(R.id.button_blue);
        button_red = (Button) findViewById(R.id.button_red);
        button_yellow = (Button) findViewById(R.id.button_yellow);
        button_blue.setOnClickListener(this);
        button_yellow.setOnClickListener(this);
        button_red.setOnClickListener(this);
        yellow = "Желтые";
        blue = "Синие";
        red = "Красные";
        Intent intent = getIntent();

        String editTextName = intent.getStringExtra("name");

        tv_Name.setText("Вас зовут " + editTextName);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.button_blue:
                Intent intent = new Intent(this, ActivityThree.class);
                intent.putExtra("name", tv_Name.getText().toString());
                intent.putExtra("color", blue);
                startActivity(intent);
                break;
            case R.id.button_red:
                Intent intent_red = new Intent(this, ActivityThree.class);
                intent_red.putExtra("name", tv_Name.getText().toString());
                intent_red.putExtra("color", red);
                startActivity(intent_red);
                break;
            case R.id.button_yellow:
                Intent intent_yellow = new Intent(this, ActivityThree.class);
                intent_yellow.putExtra("name", tv_Name.getText().toString());
                intent_yellow.putExtra("color", yellow);
                startActivity(intent_yellow);
                break;
            default:
                break;


        }
    }
}